export default array => {
  let arr = array.slice(0);
  for (let i = 0; i < arr.length; i += 2) {
    if (i > 0 && arr[i - 1] > arr[i]) {
      [arr[i - 1], arr[i]] = [arr[i], arr[i - 1]];
    }
    if (i < arr.length && arr[i] < arr[i + 1]) {
      [arr[i], arr[i + 1]] = [arr[i + 1], arr[i]];
    }
  }
  return arr;
};
