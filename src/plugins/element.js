import Vue from "vue";
import {
  Button,
  Container,
  Main,
  Row,
  Col,
  Form,
  FormItem,
  Input
} from "element-ui";
import lang from "element-ui/lib/locale/lang/en";
import locale from "element-ui/lib/locale";
import "element-ui/lib/theme-chalk/reset.css";

locale.use(lang);

Vue.use(Button);
Vue.use(Container);
Vue.use(Main);
Vue.use(Row);
Vue.use(Col);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Input);
