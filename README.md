# vue-sort-wave
[![pipeline status](https://gitlab.com/jankrloz/vue-sort-wave/badges/master/pipeline.svg)](https://gitlab.com/jankrloz/vue-sort-wave/commits/master)

[DEMO (https://jankrloz.gitlab.io/vue-sort-wave/)](https://jankrloz.gitlab.io/vue-sort-wave/)

[Algorithm (wave.sort.js)](src/wave.sort.js)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
